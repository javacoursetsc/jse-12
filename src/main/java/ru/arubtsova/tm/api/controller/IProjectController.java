package ru.arubtsova.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectById();

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

    void startProjectByIndex();

    void startProjectById();

    void startProjectByName();

    void finishProjectByIndex();

    void finishProjectById();

    void finishProjectByName();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByName();

}
